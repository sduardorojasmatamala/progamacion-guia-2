#se debe crear un codigo el cual defrife este string incriptado 
#¡XeXgXaXsXsXeXm XtXeXrXcXeXs Xa XsXi XsXiXhXt
print("El mensaje incriptado es el siguiente:")
print("¡XeXgXaXsXsXeXm XtXeXrXcXeXs Xa XsXi XsXiXhXt")
#se crea una variable que contenga el mensaje incriptado 
clave = "¡XeXgXaXsXsXeXm XtXeXrXcXeXs Xa XsXi XsXiXhXt"
#se eliminan las X ya que son innecesarias 
clavex = clave.replace("X", "")
#se muestra como queda el mensaje sin las X
print("\nAsi queda el mensaje sin las X:")
print(clavex)
#el mensaje se da vuelta para poder entenderlo mejor 
newclave = "".join(reversed(clavex))
print("\nEl significado del mensaje incriptado es el sigiente:")
print(newclave)